# PHP-FPM

Version: 7.4

Copy .env.example to .env

```
cp .env.example .env
```

Modify variables:

* USER_GROUP : Specify user and group PHP

Build: 

```
docker-compose build
```

